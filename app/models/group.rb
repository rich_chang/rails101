# 做跟資料庫有關的動作
class Group < ActiveRecord::Base
	validates :title, presence: true #設定title這個欄位必填，不能空白

	has_many :posts #建立group與post之間model的關聯性
	has_many :group_users
	has_many :members, through: :group_users, source: :user

	#這邊的 :owner 等同於 :user
	#:owner這個欄位可以任意指定名稱，但不管什麼名稱都還是對應到User這個model
	belongs_to :owner, class_name: "User", foreign_key: :user_id
end
