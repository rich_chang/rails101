class Post < ActiveRecord::Base
	belongs_to :group #建立group與post之間model的關聯性
	validates :content, presence: true
end
